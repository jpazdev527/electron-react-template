import React, { Component } from 'react';
const { ipcRenderer } = window.require('electron');

class Home extends Component {
  onClickHandler = (event) => {
    ipcRenderer.send('signal1', 'ping');
  }

  componentDidMount = () => {
    ipcRenderer.on('signal1', (event, arg) => {
      new Notification('Info', { body: arg })
    });
  }

  render() {
    return (
      <>
        <h1>Home</h1>
        <button onClick={this.onClickHandler}>Click Me!</button>
      </>
    )
  }
}

export default Home;
