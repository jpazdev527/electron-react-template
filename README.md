
# [Electron + React Boilerplate](#electron--react-boilerplate)
- [Electron + React Boilerplate](#electron--react-boilerplate)
  - [Description](#description)
  - [Running the Project](#running-the-project)
  - [Technology Stack](#technology-stack)
    - [electron (*Dev Dependency*)](#electron-dev-dependency)
    - [electron-builder (*Dev Dependency*)](#electron-builder-dev-dependency)
    - [react](#react)
    - [create-react-app](#create-react-app)
    - [create-react-component-folder](#create-react-component-folder)
    - [react-router-dom](#react-router-dom)
    - [react-redux](#react-redux)
    - [redux](#redux)
    - [redux-thunk](#redux-thunk)
    - [foreman (*Dev Dependency*)](#foreman-dev-dependency)

## Description
A basic boilerplate project for creating desktop applications with electron [electron](https://electronjs.org) & [react](https://reactjs.org).
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Running the Project
1. Clone the project into your computer.
2. Run `npm install`.
3. Run `npm run dev`.


## Technology Stack
A brief description of all the major modules being used throughout the project.

### [electron](https://electronjs.org) (*Dev Dependency*)
Build cross platform desktop apps with JavaScript, HTML, and CSS. You can watch an overview of what electron offers through the following [video](https://www.youtube.com/watch?v=8YP_nOCO-4Q&feature=youtu.be).

### [electron-builder](https://github.com/electron-userland/electron-builder) (*Dev Dependency*)
A complete solution to package and build a ready for distribution Electron, Proton Native app for macOS, Windows and Linux with “auto update” support out of the box.

### [react](https://reactjs.org)
A JavaScript library for building user interfaces.

### [create-react-app](https://create-react-app.dev)
Set up a modern web app by running one command.

### [create-react-component-folder](https://www.npmjs.com/package/create-react-component-folder)
It creates React or React Native component folder structure with one command.
There is also support for Typescript, React Native, Less and Sass.

### [react-router-dom](https://www.npmjs.com/package/react-router-dom)
DOM bindings for React Router.

### [react-redux](https://react-redux.org)
Official React bindings for Redux. Facilitates the use of redux within react applications.

### [redux](https://redux.js.org)
A predictable state container for JavaScript apps.

### [redux-thunk](https://github.com/reduxjs/redux-thunk)
A middleware that extend the redux store's abilities, and let you write async logic that interacts with the store.

### [foreman](https://www.npmjs.com/package/foreman) (*Dev Dependency*)
Foreman is a manager for Procfile-based applications. Its aim is to abstract away the details of the Procfile format, and allow you to either run your application directly or export it to some other process management format.<br />Foreman is being used to first run the **react** server and then start the **electron** service.
